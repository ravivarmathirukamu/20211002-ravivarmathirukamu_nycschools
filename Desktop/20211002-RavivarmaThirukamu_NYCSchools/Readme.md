This app contains two screens for users to see NYC Schools list and SAT score. 
First screen gives the list of NYC Schools. 
The user can click on any school from the list and navigate to next screen to see the SAT score information for the selected school. 
This app supports portrait mode in iPhone and iPads with minimum deployment target as iOS 12.

Technical details and guidelines:
    1. It follows MVVM architecture 
    2. A UIViewController should have a ViewModel class. Business logics are implemented in the ViewModel class. And handed over the data to ViewController.
    3. ViewController is responsible for updating the UI with the data provided by ViewModel.
    4. Network_Layer - All the REST api implementations goes into this directory.
    5. Unit test classes added for the appropriate view models. Code coverage is >= 85%.


