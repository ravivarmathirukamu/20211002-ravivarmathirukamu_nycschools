//
//  SchoolInfoViewModelTests.swift
//  20211002-RavivarmaThirukamu_NYCSchoolsTests
//
//  Created by Ravivarma thirukamu on 10/3/21.
//

import XCTest
@testable import _0211002_RavivarmaThirukamu_NYCSchools

class SchoolInfoViewModelTests: XCTestCase {

    var viewModel: SchoolInfoViewModel?
    
    override func setUp() {
        super.setUp()
        viewModel?.apiService = MockSchoolInfoApiService()
        viewModel = SchoolInfoViewModel(with: "21K728", schoolInfoDataModel: MockSchoolInfoDataModel.fetchSchoolInfoDataModel())
    }
    
    func testViewModelSchoolInfo_Success() {
        let exp = expectation(description: "Should return info")
        viewModel?.fetchSchoolInfo(completion: { info, error in
            self.viewModel?.satInfo = info ?? []
            self.viewModel?.createArrayForSatScore()
            let critical = self.viewModel?.getItemsAtIndexPath(indexPath: 0)
            let math = self.viewModel?.getItemsAtIndexPath(indexPath: 1)
            let writing = self.viewModel?.getItemsAtIndexPath(indexPath: 2)
            XCTAssertNotNil(info)
            XCTAssertEqual(critical, "411")
            XCTAssertEqual(math, "369")
            XCTAssertEqual(writing, "373")
            exp.fulfill()
        })
        wait(for: [exp], timeout: 5.0)
    }
    
    func testViewModelFetchSchoolList_Failure() {
        let mockService = MockSchoolListApiService()
        mockService.error = .serviceError
        viewModel?.apiService = mockService
        let exp = expectation(description: "Should return error")
        viewModel?.fetchSchoolInfo(completion: { info, error in
            XCTAssertNil(info)
            XCTAssertNotNil(error)
            exp.fulfill()
        })
        wait(for: [exp], timeout: 5.0)
    }

}

class MockSchoolInfoApiService: Api {
    
    var error: RequestError?
    
    func fetch(url: URL, httpMethod: HttpMethod, callback: @escaping CompletionHandler) {
        if let err = error {
            callback(.failure(err))
        } else {
            if let filePath = Bundle.main.path(forResource: "MockSchoolInfo_Success_Response", ofType: "json") {
                do {
                    let mockResponseStr = try String(contentsOfFile: filePath)
                    let mockResponse = mockResponseStr.data(using: .utf8)
                    
                    if let json = mockResponse {
                        callback(.success(json as Any))
                    }
                } catch { }
            }
        }
    }
    
}

struct MockSchoolInfoDataModel {
    static func fetchSchoolInfoDataModel() -> SchoolInfoDataModel {
        return SchoolInfoDataModel(schoolName: "Liberation Diploma Plus High School", address: "2865 West 19th Street", city: "Brooklyn", zip: "11224", count: "206", state: "NY")
    }
}
