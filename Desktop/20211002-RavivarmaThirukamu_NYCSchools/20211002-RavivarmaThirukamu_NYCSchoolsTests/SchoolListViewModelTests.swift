//
//  SchoolListViewModelTests.swift
//  20211002-RavivarmaThirukamu_NYCSchoolsTests
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import XCTest
@testable import _0211002_RavivarmaThirukamu_NYCSchools

class SchoolListViewModelTests: XCTestCase {
    
    var viewModel: SchoolListViewModel?

    override func setUp() {
        super.setUp()
        viewModel = SchoolListViewModel()
        viewModel?.apiService = MockSchoolListApiService()
    }
    
    func testViewModelFetchSchoolList_Success() {
        let exp = expectation(description: "Should return list")
        viewModel?.fetchSchoolList(completion: { (list, error) in
            self.viewModel?.schoolList = list ?? []
            let items = self.viewModel?.getItemsAtIndexPath(indexPath: 1)
            let rows = self.viewModel?.getNumberOfRows()
            XCTAssertNotNil(list)
            XCTAssertEqual(3, rows)
            XCTAssertEqual(items?.name, "Liberation Diploma Plus High School")
            XCTAssertEqual(items?.city, "Brooklyn")
            exp.fulfill()
        })
        wait(for: [exp], timeout: 5.0)
    }
    
    func testViewModelFetchSchoolList_Failure() {
        let mockService = MockSchoolListApiService()
        mockService.error = .serviceError
        viewModel?.apiService = mockService
        let exp = expectation(description: "Should return error")
        viewModel?.fetchSchoolList(completion: { (list, error) in
            XCTAssertNil(list)
            XCTAssertNotNil(error)
            exp.fulfill()
        })
        wait(for: [exp], timeout: 5.0)
    }

}

class MockSchoolListApiService: Api {
    
    var error: RequestError?
    
    func fetch(url: URL, httpMethod: HttpMethod, callback: @escaping CompletionHandler) {
        if let err = error {
            callback(.failure(err))
        } else {
            if let filePath = Bundle.main.path(forResource: "MockSchoolList_Success_Response", ofType: "json") {
                do {
                    let mockResponseStr = try String(contentsOfFile: filePath)
                    let mockResponse = mockResponseStr.data(using: .utf8)
                    
                    if let json = mockResponse {
                        callback(.success(json as Any))
                    }
                } catch { }
            }
        }
    }
    
}
