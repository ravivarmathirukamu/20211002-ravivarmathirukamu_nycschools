//
//  SchoolList.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import Foundation

public struct SchoolListDataModel: Codable {
    public let name: String
    public let city: String
    public let dbn: String
    public let address: String
    public let zip: String
    public let state: String
    public let count: String
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case city = "city"
        case dbn = "dbn"
        case address = "primary_address_line_1"
        case zip = "zip"
        case state = "state_code"
        case count = "total_students"
    }
}
