//
//  SchoolInfo.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import Foundation

public struct SatScoreDataModel: Codable {
    public let name: String
    public let critical: String
    public let math: String
    public let writing: String
    public let satCount: String
    enum CodingKeys: String, CodingKey {
        case name = "school_name"
        case critical = "sat_critical_reading_avg_score"
        case math = "sat_math_avg_score"
        case writing = "sat_writing_avg_score"
        case satCount = "num_of_sat_test_takers"
    }
}

public struct SchoolInfoDataModel {
    public let schoolName: String
    public let address: String
    public let city: String
    public let zip: String
    public let count: String
    public let state: String
}
