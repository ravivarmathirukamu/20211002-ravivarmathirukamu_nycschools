//
//  ApiService.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import Foundation

enum HttpMethod: String {
    case GET
    case POST
}

enum RequestError: Error {
    case serviceError
    case noData
}

protocol Api {
    func fetch(url: URL, httpMethod: HttpMethod, callback: @escaping CompletionHandler)
}

typealias CompletionHandler = (Result<Any, RequestError>) -> Void

class ApiService: Api {
    
    var session: URLSession = URLSession.shared
    
    func fetch(url: URL, httpMethod: HttpMethod, callback: @escaping CompletionHandler) {
        
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.rawValue
        let data = session.dataTask(with: request) { data, response, error in
            guard error == nil else {
                print("No data")
                callback(.failure(.noData))
                return
            }
            guard let response = response as? HTTPURLResponse, 200...209 ~= response.statusCode else {
                print("Service error")
                callback(.failure(.serviceError))
                return
            }
            callback(.success(data as Any))

        }
        data.resume()
    }
    
}
