//
//  AppStyle.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import Foundation
import UIKit

struct AppStyle {
    
    static let blueColor = UIColor(red: 13/250, green: 152/250, blue: 186/250, alpha: 1)
    static let blackColor = UIColor(red: 0/250, green: 0/250, blue: 0/250, alpha: 1)
    static let greenColor = UIColor(red: 0/250, green: 187/250, blue: 117/250, alpha: 1)
    static let redColor = UIColor(red: 191/250, green: 11/250, blue: 14/250, alpha: 1)
    static let darkGreyColor = UIColor(red: 169/250, green: 169/250, blue: 169/250, alpha: 1)
    static let lightGreyColor = UIColor(red: 211/250, green: 211/250, blue: 211/250, alpha: 1)
    static let navColor = UIColor(red: 34/255, green: 136/255, blue: 0, alpha: 1.0)
    
    static func headerLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: Constants.labelFontSize15)
        label.numberOfLines = Constants.zeroInt
        label.textColor = blueColor
        label.textAlignment = .left
        return label
    }
    
    static func titleLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: Constants.labelFontSize15)
        label.numberOfLines = Constants.zeroInt
        label.textColor = blueColor
        label.textAlignment = .left
        return label
    }
    
    static func subtitleLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: Constants.labelFontSize15)
        label.numberOfLines = Constants.zeroInt
        label.textColor = redColor
        label.textAlignment = .left
        return label
    }
    
    static func smallFontLabel() -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: Constants.labelFontSize12)
        label.numberOfLines = Constants.zeroInt
        label.textColor = darkGreyColor
        label.textAlignment = .left
        return label
    }
    
    static func spinner() -> UIActivityIndicatorView {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.backgroundColor = .white
        spinner.color = redColor
        spinner.hidesWhenStopped = true
        return spinner
    }
    
    static func separator() -> UIView {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = lightGreyColor
        return view
    }
    
}
