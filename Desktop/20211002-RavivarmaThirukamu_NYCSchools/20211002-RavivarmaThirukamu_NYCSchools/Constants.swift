//
//  Constants.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import Foundation
import UIKit

struct Constants {
    static let zeroInt: Int = 0
    static let zeroCgFloat: CGFloat = 0
    static let labelFontSize12: CGFloat = 12
    static let labelFontSize15: CGFloat = 15
    static let verticalConstraint:CGFloat = 24
    static let horizontalConstraint: CGFloat = 32
    static let verticalSpacing: CGFloat = 8
    static let horizontalSpacing: CGFloat = 8
    static let verticalSpacingSmall: CGFloat = 4
    static let separatorSpacing:CGFloat = 12
    static let separatorThin: CGFloat = 1
    static let separatorThick: CGFloat = 2
    static let genericErrorTitle = "Error"
    static let genericErrorMessage = "Network Unavailable"
    static let alertOk = "OK"
    static let nilValue = "N/A"
}
