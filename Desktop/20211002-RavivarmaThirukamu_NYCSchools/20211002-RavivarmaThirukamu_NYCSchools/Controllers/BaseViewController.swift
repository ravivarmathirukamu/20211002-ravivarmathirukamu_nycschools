//
//  BaseViewController.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import UIKit

class BaseViewController: UIViewController {

    lazy var spinner:UIActivityIndicatorView = AppStyle.spinner()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        setupSpinner()
    }
    
    func setupSpinner() {
        view.addSubview(spinner)
        spinner.startAnimating()
    
        //constraints
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: spinner, attribute: .centerX, relatedBy: .equal, toItem:self.view, attribute: .centerX, multiplier: 1.0, constant: Constants.zeroCgFloat),
            NSLayoutConstraint(item: spinner, attribute: .centerY, relatedBy: .equal, toItem:self.view, attribute: .centerY, multiplier: 1.0, constant: Constants.zeroCgFloat)
        ])
    }
    
    func showPopup(title: String, message: String) {
        self.showAlert(title: title, message: message, actionTitles: [Constants.alertOk], actions: [ { _ in
            // UIAlertActions can be implemented here
        }
        ])
    }

}
