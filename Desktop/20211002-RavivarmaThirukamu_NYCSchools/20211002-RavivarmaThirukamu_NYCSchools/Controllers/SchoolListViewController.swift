//
//  SchoolListViewController.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import UIKit

class SchoolListViewController: BaseViewController {

    private let screenTitle = "NYC Schools"
    private let cellId = "SchoolListCell"
    private let tableViewEstimatedHeight: CGFloat = 100
    var schoolListTableView = UITableView()
    var viewModel = SchoolListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.title = screenTitle
        setupTableView()
        setupSpinner()
        fetchData()
    }
    
    private func setupTableView() {
        view.addSubview(schoolListTableView)
        schoolListTableView.delegate = self
        schoolListTableView.dataSource = self
        schoolListTableView.separatorStyle = .none
        schoolListTableView.estimatedRowHeight = tableViewEstimatedHeight
        schoolListTableView.rowHeight = UITableView.automaticDimension
        schoolListTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            schoolListTableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor, constant: Constants.zeroCgFloat),
            schoolListTableView.leadingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leadingAnchor),
            schoolListTableView.trailingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.trailingAnchor),
            schoolListTableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor)
        ])
        schoolListTableView.register(SchoolListTableViewCell.self, forCellReuseIdentifier: cellId)
    }
    
    private func fetchData() {
        viewModel.fetchSchoolList { (list, error) in
            if error == nil {
                DispatchQueue.main.async { [weak self] in
                    self?.spinner.stopAnimating()
                    self?.schoolListTableView.reloadData()
                }
            } else {
                DispatchQueue.main.async {[weak self] in
                    self?.spinner.stopAnimating()
                    self?.showPopup(title: Constants.genericErrorTitle, message: Constants.genericErrorMessage)
                }
            }
        }
    }

}

extension SchoolListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? SchoolListTableViewCell  else {
            return UITableViewCell()
        }
        let info = viewModel.getItemsAtIndexPath(indexPath: indexPath.row)
        cell.configureCellWithData(name: info?.name ?? "", address: info?.city ?? "")
        cell.selectionStyle = .none
        return cell
    }
}

extension SchoolListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let info = viewModel.schoolList[indexPath.row]
        let vc = SchoolInfoViewController()
        vc.viewModel = SchoolInfoViewModel(with: info.dbn, schoolInfoDataModel: SchoolInfoDataModel(schoolName: info.name, address: info.address, city: info.city, zip: info.zip, count: info.count, state: info.state))
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
