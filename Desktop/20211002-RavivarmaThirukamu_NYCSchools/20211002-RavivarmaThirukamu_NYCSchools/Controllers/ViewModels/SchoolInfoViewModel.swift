//
//  SchoolInfoViewModel.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import Foundation

class SchoolInfoViewModel {
    
    var apiService: Api = ApiService()
    var schoolInfoUrl = ""
    let schoolInfoBaseUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn="
    var params: String?
    var schoolInfoDataModel: SchoolInfoDataModel?
    var satInfo = [SatScoreDataModel]()
    var scoreArray: [String] = ["N/A", "N/A", "N/A"]
    
    init(with params: String, schoolInfoDataModel: SchoolInfoDataModel?) {
        self.params = params
        self.schoolInfoDataModel = schoolInfoDataModel
        self.schoolInfoUrl = schoolInfoBaseUrl+params
    }
    
    func fetchSchoolInfo(completion: @escaping (_ info: [SatScoreDataModel]?, _ error: Error?) -> Void) {
        
        guard let url = URL(string: schoolInfoUrl) else {
            return
        }
        
        apiService.fetch(url: url, httpMethod: HttpMethod.GET) { (result) in
            switch result {
            case .success(let data):
                if let dataObj = data as? Data {
                    let info = try? JSONDecoder().decode([SatScoreDataModel].self, from: dataObj)
                    self.satInfo = info ?? []
                    self.createArrayForSatScore()
                    completion(info, nil)
                }
            case .failure(let error):
                print("Error fetching data")
                completion(nil, error)
                
            }
        }
    }
    
    func createArrayForSatScore() {
        let _ = satInfo.first.map {
            scoreArray[0] = ($0.critical)
            scoreArray[1] = ($0.math)
            scoreArray[2] = ($0.writing)
        }
    }
    
    func getItemsAtIndexPath(indexPath: Int) -> String {
        return scoreArray[indexPath]
    }
}
