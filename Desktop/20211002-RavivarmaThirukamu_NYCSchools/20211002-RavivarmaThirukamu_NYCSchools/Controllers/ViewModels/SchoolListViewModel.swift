//
//  SchoolListViewModel.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import Foundation

class SchoolListViewModel {
    
    var apiService: Api = ApiService()
    let schoolListUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    var schoolList = [SchoolListDataModel]()
    
    func fetchSchoolList(completion: @escaping (_ list: [SchoolListDataModel]?, _ error: Error?) -> Void) {
        
        guard let url = URL(string: schoolListUrl) else {
            return
        }
        
        apiService.fetch(url: url, httpMethod: HttpMethod.GET) { (result) in
            switch result {
            case .success(let data):
                if let dataObj = data as? Data {
                    let info = try? JSONDecoder().decode([SchoolListDataModel].self, from: dataObj)
                    self.schoolList = info ?? []
                    completion(info, nil)
                }
            case .failure(let error):
                print("Error fetching data")
                completion(nil, error)
                
            }
        }
    }
    
    func getNumberOfRows() -> Int {
        return schoolList.count
    }
    
    func getItemsAtIndexPath(indexPath: Int) -> SchoolListDataModel? {
        return schoolList[indexPath]
    }
    
}
