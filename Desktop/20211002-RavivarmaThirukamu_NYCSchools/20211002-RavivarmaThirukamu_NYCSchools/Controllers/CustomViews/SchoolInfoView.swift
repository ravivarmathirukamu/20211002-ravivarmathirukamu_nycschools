//
//  SchoolInfoView.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/3/21.
//

import UIKit

class SchoolInfoView: UIView {
    
    let schoolNameLabel: UILabel = AppStyle.headerLabel()
    let addressLabel1: UILabel = AppStyle.smallFontLabel()
    let cityLabel: UILabel = AppStyle.smallFontLabel()
    let studentCountLabel: UILabel = AppStyle.subtitleLabel()
    let satCountLabel: UILabel = AppStyle.subtitleLabel()
    let separator: UIView = AppStyle.separator()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupViews() {
        addSubview(schoolNameLabel)
        addSubview(addressLabel1)
        addSubview(cityLabel)
        addSubview(studentCountLabel)
        addSubview(satCountLabel)
        addSubview(separator)
        separator.backgroundColor = AppStyle.blackColor
        NSLayoutConstraint.activate([
            separator.heightAnchor.constraint(equalToConstant: Constants.separatorThick),
            separator.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.separatorSpacing),
            separator.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.separatorSpacing),
            separator.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Constants.zeroCgFloat),
            
            schoolNameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraint),
            schoolNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: Constants.verticalConstraint),
            schoolNameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraint),
            
            addressLabel1.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraint),
            addressLabel1.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: Constants.verticalSpacing),
            addressLabel1.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraint),
            
            cityLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraint),
            cityLabel.topAnchor.constraint(equalTo: addressLabel1.bottomAnchor, constant: Constants.verticalSpacingSmall),
            cityLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraint),
            
            studentCountLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraint),
            studentCountLabel.topAnchor.constraint(equalTo: cityLabel.bottomAnchor, constant: Constants.verticalSpacing),
            studentCountLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraint),
            
            satCountLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraint),
            satCountLabel.topAnchor.constraint(equalTo: studentCountLabel.bottomAnchor, constant: Constants.verticalSpacingSmall),
            satCountLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraint),
            satCountLabel.bottomAnchor.constraint(equalTo: separator.topAnchor, constant: -Constants.verticalConstraint)
            ])
    }
    
    func configureViewWithData(dataModel: SchoolInfoDataModel?, satCount: String) {
        schoolNameLabel.text = dataModel?.schoolName
        addressLabel1.text = dataModel?.address
        cityLabel.text = (dataModel?.city ?? "") + ", " + (dataModel?.state ?? "") + " - " + (dataModel?.zip ?? "")
        studentCountLabel.text = "Number of students: \(dataModel?.count ?? "")"
        satCountLabel.text = "Number of SAT test takers: \(satCount)"
    }
}
