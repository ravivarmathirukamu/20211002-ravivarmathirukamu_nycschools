//
//  SchoolInfoTableViewHeader.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/3/21.
//

import UIKit

class SchoolInfoTableViewHeader: UITableViewHeaderFooterView {
    
    let title: UILabel = AppStyle.headerLabel()

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setUpView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpView() {
        addSubview(title)
        NSLayoutConstraint.activate([
            title.topAnchor.constraint(equalTo: topAnchor, constant: Constants.verticalSpacing),
            title.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.horizontalConstraint),
            title.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Constants.horizontalConstraint),
            title.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Constants.zeroCgFloat)
        ])
    }
    
}
