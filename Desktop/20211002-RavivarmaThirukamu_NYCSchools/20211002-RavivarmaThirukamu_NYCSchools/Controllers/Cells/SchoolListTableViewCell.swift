//
//  SchoolListTableViewCell.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import UIKit

class SchoolListTableViewCell: UITableViewCell {

    let schoolNameLabel: UILabel = AppStyle.titleLabel()
    let addressLabel: UILabel = AppStyle.subtitleLabel()
    let separator: UIView = AppStyle.separator()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpViews() {
        contentView.addSubview(schoolNameLabel)
        contentView.addSubview(addressLabel)
        contentView.addSubview(separator)
        
        //Add constraints
        NSLayoutConstraint.activate([
            schoolNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.horizontalConstraint),
            schoolNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.verticalConstraint),
            schoolNameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.horizontalConstraint),
            
            addressLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.horizontalConstraint),
            addressLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.horizontalConstraint),
            addressLabel.topAnchor.constraint(equalTo: schoolNameLabel.bottomAnchor, constant: Constants.verticalSpacing),
            
            separator.heightAnchor.constraint(equalToConstant: Constants.separatorThin),
            separator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.separatorSpacing),
            separator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.separatorSpacing),
            separator.topAnchor.constraint(equalTo: addressLabel.bottomAnchor, constant: Constants.verticalConstraint),
            separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Constants.zeroCgFloat)
        ])
    }
    
    func configureCellWithData(name: String, address: String) {
        schoolNameLabel.text = nil
        addressLabel.text = nil
        schoolNameLabel.text = name
        addressLabel.text = address
    }

}
