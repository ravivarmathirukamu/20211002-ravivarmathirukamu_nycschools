//
//  SchoolInfoTableViewCell.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/3/21.
//

import UIKit

class SchoolInfoTableViewCell: UITableViewCell {
    
    let titleLabel: UILabel = AppStyle.titleLabel()
    let descLabel: UILabel = AppStyle.subtitleLabel()
    let separator: UIView = AppStyle.separator()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUpViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUpViews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descLabel)
        contentView.addSubview(separator)
        
        //Add constraints
        NSLayoutConstraint.activate([
            separator.heightAnchor.constraint(equalToConstant: Constants.separatorThin),
            separator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.separatorSpacing),
            separator.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.separatorSpacing),
            separator.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Constants.zeroCgFloat),
            
            titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: Constants.horizontalConstraint),
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.verticalConstraint),
            titleLabel.bottomAnchor.constraint(equalTo: separator.topAnchor, constant: -Constants.verticalConstraint),
            
            descLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -Constants.horizontalConstraint),
            descLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.verticalConstraint),
            descLabel.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: Constants.horizontalSpacing),
            descLabel.bottomAnchor.constraint(equalTo: separator.topAnchor, constant: -Constants.verticalConstraint)
        ])
    }
    
    func configureCellWithData(title: String, score: String) {
        titleLabel.text = nil
        descLabel.text = nil
        descLabel.textAlignment = .right
        titleLabel.text = title
        descLabel.text = score
    }

}
