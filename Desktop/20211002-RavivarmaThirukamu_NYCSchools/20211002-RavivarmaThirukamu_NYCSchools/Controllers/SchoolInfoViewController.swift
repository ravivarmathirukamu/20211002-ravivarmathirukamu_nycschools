//
//  SchoolInfoViewController.swift
//  20211002-RavivarmaThirukamu_NYCSchools
//
//  Created by Ravivarma thirukamu on 10/2/21.
//

import UIKit

class SchoolInfoViewController: BaseViewController, UITableViewDelegate {
    
    private let screenTitle = "Details"
    private let cellId = "SchoolInfoCell"
    private let tableViewEstimatedHeight: CGFloat = 50
    var viewModel: SchoolInfoViewModel?
    var topContainerView = SchoolInfoView()
    var schoolInfoTableView = UITableView()
    let titleArray = ["Critical", "Math", "Writing"]

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.title = screenTitle
        setupTopView()
        setupTableView()
        fetchData()
        setupSpinner()
    }
    
    private func setupTopView() {
        view.addSubview(topContainerView)
        topContainerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            topContainerView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor, constant: Constants.zeroCgFloat),
            topContainerView.leadingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leadingAnchor),
            topContainerView.trailingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.trailingAnchor)
        ])
        
    }
    
    private func setupTableView() {
        view.addSubview(schoolInfoTableView)
        schoolInfoTableView.delegate = self
        schoolInfoTableView.dataSource = self
        schoolInfoTableView.separatorStyle = .none
        schoolInfoTableView.estimatedRowHeight = tableViewEstimatedHeight
        schoolInfoTableView.rowHeight = UITableView.automaticDimension
        schoolInfoTableView.estimatedSectionHeaderHeight = tableViewEstimatedHeight
        schoolInfoTableView.sectionHeaderHeight = UITableView.automaticDimension
        schoolInfoTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            schoolInfoTableView.topAnchor.constraint(equalTo: topContainerView.bottomAnchor, constant: Constants.zeroCgFloat),
            schoolInfoTableView.leadingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leadingAnchor),
            schoolInfoTableView.trailingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.trailingAnchor),
            schoolInfoTableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor)
        ])
        schoolInfoTableView.register(SchoolInfoTableViewCell.self, forCellReuseIdentifier: cellId)
        schoolInfoTableView.register(SchoolInfoTableViewHeader.self, forHeaderFooterViewReuseIdentifier: "header")
    }
    
    private func fetchData() {
        viewModel?.fetchSchoolInfo { (list, error) in
            if error == nil {
                DispatchQueue.main.async { [weak self] in
                    self?.spinner.stopAnimating()
                    self?.updateTopView()
                    self?.schoolInfoTableView.reloadData()
                }
            } else {
                DispatchQueue.main.async {[weak self] in
                    self?.spinner.stopAnimating()
                    self?.showPopup(title: Constants.genericErrorTitle, message: Constants.genericErrorMessage)
                }
            }
        }
    }
    
    private func updateTopView() {
        topContainerView.configureViewWithData(dataModel: viewModel?.schoolInfoDataModel!, satCount: viewModel?.satInfo.first?.satCount ?? Constants.nilValue)
    }
    
}

extension SchoolInfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.scoreArray.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? SchoolInfoTableViewCell  else {
            return UITableViewCell()
        }
        let title = titleArray[indexPath.row]
        let score = viewModel?.getItemsAtIndexPath(indexPath: indexPath.row) ?? Constants.nilValue
        cell.configureCellWithData(title: title, score: score)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? SchoolInfoTableViewHeader else {
            return UITableViewHeaderFooterView()
        }
        headerView.contentView.backgroundColor = .white
        headerView.title.text = "Average SAT Score:"
        headerView.title.textColor = AppStyle.blackColor
        return headerView
    }
}
